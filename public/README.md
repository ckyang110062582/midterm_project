# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : CHATatNIGHT
* Key functions (add/delete)
    1. 使用者一對一私聊
    2. 所有使用者公開群聊
    
* Other functions (add/delete)
    1. 新使用者列表
    2. 上線中使用者列表

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Hosting|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-3db26.firebaseapp.com/index.html

# Components Description : 
1. 註冊與登入 : 使用者在註冊之後即會自動登入，導向聊天室(支援Google登入)
2. 登出 : 登出紐位於聊天室右上方的下拉式選單
3. 私人聊天室 : 使用者透過點擊右側使用者列表可以開啟和他人的一對一私聊(和自己聊天也是可以的)
4. 公開交流群 : 點擊左上方的"+"號按鈕可以進入公開群，能夠參與社群討論

# Other Functions Description(1~10%) : 
1. 新加入的使用者列表 : 動態列出最新加入的五位使用者
2. 上線中使用者列表 : 動態更新上線中使用者，方便使用者快速找到聊天對象
3. 閱讀友善設計 : 聊天室設計貼近真實通訊軟體，訊息量大時會自動捲動到底部方便閱讀


## Security Report (Optional)

1. 註冊安全機制加強 : 密碼double check、username不可含有非法字元
2. 網頁自動跳轉 : 若使用者沒有登入就進入聊天室即會自動跳轉回登入頁面
3. 輸入保護 : 使用replace function將使用者輸入的html tag過濾掉，防止網頁出現問題